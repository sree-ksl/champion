package com.ksl.pc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ksl.pc.model.GetApp;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Q1Activity extends AppCompatActivity {

    ProgressDialog progressDialog;

    private static final String BASE_URL = "https://api.quickbase.com/v1/records/query/";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.q1);

        progressDialog = new ProgressDialog(Q1Activity.this);
        progressDialog.setMessage("Loading....");
//        progressDialog.show();

        Button acceptBtn = findViewById(R.id.acceptbtn);
        Button askParents = findViewById(R.id.askParentsBtn);

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tvOption = findViewById(R.id.tvOption);

                tvOption.setVisibility(View.VISIBLE);
                tvOption.setText("You pack and set out together with Raj. After crossing the border, Raj keeps your documents (including passport) saying its safer with him.");
                try {
                    progressDialog.show();
                    Thread.sleep(2000);
                    Intent q2 = new Intent(Q1Activity.this, Q2Activity.class);
                    startActivity(q2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        askParents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tvOption = findViewById(R.id.tvOption);
                tvOption.setVisibility(View.VISIBLE);
                tvOption.setText("They want to meet Raj. I invite him to meet them, they end up liking him and give me money for the trip.");

//                try {
//                    Thread.sleep(3000);
////                    tvOption.setVisibility(View.INVISIBLE);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }

            }
        });

//      Query for records
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceholderApi jsonPlaceholderApi = retrofit.create(JsonPlaceholderApi.class);
        Call<List<GetApp>> call = jsonPlaceholderApi.getApp();

        call.enqueue(new Callback<List<GetApp>>() {
            @Override
            public void onResponse(Call<List<GetApp>> call, Response<List<GetApp>> response) {

                if(!response.isSuccessful()){
//                    tvResult.setText("Code: " +response.code());
                    return;
                }

                List<GetApp> getApps = response.body();
                for(GetApp getApp : getApps) {
                    String content = "";
                    content += "ID: " + getApp.getId() + "\n";
                    content += "Name: " + getApp.getName() + "\n";
                    content += "Description: " + getApp.getDescription();

//                   tvResult.append(content);
                }

            }

            @Override
            public void onFailure(Call<List<GetApp>> call, Throwable t) {
//                tvResult.setText(t.getMessage());

            }
        });
    }
}
