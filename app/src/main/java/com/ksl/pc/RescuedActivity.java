package com.ksl.pc;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class RescuedActivity extends AppCompatActivity {

    Button keepBtn, helpBtn;
    TextView optiontxt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rescue);

        keepBtn = findViewById(R.id.keepBtn);
        helpBtn = findViewById(R.id.askHelp);


        keepBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optiontxt = findViewById(R.id.optionText);

                optiontxt.setVisibility(View.VISIBLE);
                optiontxt.setText("Upon leaving the exploitation ring, human trafficking victims face numerous difficulties like mental trauma, physical injuries and illness, discrimination and poverty. If they don't get help, their recovery is harder and the possibility of their rights being violated increases.");
                //TODO - helpline - go back to Thankyou page
                Intent thankyou = new Intent(RescuedActivity.this, ThankyouActivity.class);
                startActivity(thankyou);


            }
        });

        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent helpline = new Intent(RescuedActivity.this, HelplineActivity.class);
               startActivity(helpline);

            }
        });
    }
}
