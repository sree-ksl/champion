package com.ksl.pc;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ksl.pc.model.GetApp;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ProgressDialog progressDialog;

    private static final String BASE_URL = "https://api.quickbase.com/v1/apps/bqyqecugy/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_play_activity);

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading....");
//        progressDialog.show();

        Button playBtn = findViewById(R.id.playBtn);
//        final TextView tvResult = findViewById(R.id.tv1);

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    progressDialog.show();
                    Thread.sleep(2000);
                    Intent rolePlay = new Intent(MainActivity.this, Q1Activity.class);
                    startActivity(rolePlay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        //Quickbase Call
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceholderApi jsonPlaceholderApi = retrofit.create(JsonPlaceholderApi.class);
        Call<List<GetApp>> call = jsonPlaceholderApi.getApp();

//      Async callback method
        call.enqueue(new Callback<List<GetApp>>() {
            @Override
            public void onResponse(Call<List<GetApp>> call, Response<List<GetApp>> response) {

                if(!response.isSuccessful()){
//                    tvResult.setText("Code: " +response.code());
                    return;
                }

                List<GetApp> getApps = response.body();
                for(GetApp getApp : getApps) {
                   String content = "";
                   content += "ID: " + getApp.getId() + "\n";
                   content += "Name: " + getApp.getName() + "\n";
                   content += "Description: " + getApp.getDescription();

//                   tvResult.append(content);
                }

            }

            @Override
            public void onFailure(Call<List<GetApp>> call, Throwable t) {
//                tvResult.setText(t.getMessage());

            }
        });

    }
}