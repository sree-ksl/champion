package com.ksl.pc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GetApp {

    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("dateFormat")
    @Expose
    private String dateFormat;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("hasEveryoneOnTheInternet")
    @Expose
    private boolean hasEveryoneOnTheInternet;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("timeZone")
    @Expose
    private String timeZone;
    @SerializedName("updated")
    @Expose
    private String updated;

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isHasEveryoneOnTheInternet() {
        return hasEveryoneOnTheInternet;
    }

    public void setHasEveryoneOnTheInternet(boolean hasEveryoneOnTheInternet) {
        this.hasEveryoneOnTheInternet = hasEveryoneOnTheInternet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

}
