package com.ksl.pc;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ksl.pc.model.GetApp;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Q3Activity extends AppCompatActivity {

    Button escapeBtn, askClientBtn;
    TextView tvOp;

    private static final String BASE_URL = "https://api.quickbase.com/v1/records/query/";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.q3);

        escapeBtn = findViewById(R.id.escapeBtn);
        askClientBtn = findViewById(R.id.askClientBtn);


        escapeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvOp = findViewById(R.id.tvOp);
                tvOp.setVisibility(View.VISIBLE);
                tvOp.setText("The trafficker finds you. As punishment, you are not given food for several days and forces you to be with twice as many clients. Since you created problems for him, he decides to sell you to another trafficker. You remain trapped in a Sex trafficking ring, you don't know where you are and you have no one to turn to for help because you are kept locked up.");
                // TODO - helpline page intent
                try {
                    Thread.sleep(2000);
                    Intent helpline = new Intent(Q3Activity.this, HelplineActivity.class);
                    startActivity(helpline);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        askClientBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvOp = findViewById(R.id.tvOp);
                tvOp.setText("Your client takes pity on you and wants to help you get away. He tips the police and you are rescued in a raid and taken back to your home country");
                try {
                    Thread.sleep(2000);
                    Intent rescue = new Intent(Q3Activity.this, RescuedActivity.class);
                    startActivity(rescue);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });


        //      Query for records
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceholderApi jsonPlaceholderApi = retrofit.create(JsonPlaceholderApi.class);
        Call<List<GetApp>> call = jsonPlaceholderApi.getApp();

        call.enqueue(new Callback<List<GetApp>>() {
            @Override
            public void onResponse(Call<List<GetApp>> call, Response<List<GetApp>> response) {

                if(!response.isSuccessful()){
//                    tvResult.setText("Code: " +response.code());
                    return;
                }

                List<GetApp> getApps = response.body();
                for(GetApp getApp : getApps) {
                    String content = "";
                    content += "ID: " + getApp.getId() + "\n";
                    content += "Name: " + getApp.getName() + "\n";
                    content += "Description: " + getApp.getDescription();

//                   tvResult.append(content);
                }

            }

            @Override
            public void onFailure(Call<List<GetApp>> call, Throwable t) {
//                tvResult.setText(t.getMessage());

            }
        });
    }
}
