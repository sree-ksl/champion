package com.ksl.pc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ksl.pc.model.GetApp;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Q2Activity extends AppCompatActivity {

    Button yesBtn, noBtn;
    TextView tvOpt;
    ProgressDialog progressDialog;

    private static final String BASE_URL = "https://api.quickbase.com/v1/records/query/";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.q2);

        progressDialog = new ProgressDialog(Q2Activity.this);
        progressDialog.setMessage("Loading....");
//        progressDialog.show();

        yesBtn= findViewById(R.id.yesBtn);
        noBtn = findViewById(R.id.noBtn);


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvOpt = findViewById(R.id.tvOpt);
                tvOpt.setVisibility(View.VISIBLE);
                tvOpt.setText("You try to escape but Rahul catches you, beats you up and rapes you multiple times. You become a victim of human trafficking for the purpose of sexual exploitation.");
                try {
                    progressDialog.show();
                    Thread.sleep(2000);
                    Intent q3 = new Intent(Q2Activity.this, Q3Activity.class);
                    startActivity(q3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvOpt = findViewById(R.id.tvOpt);
                tvOpt.setText("Raj gets angry and beats you up, takes away your phone and money. You are scared and don't understand what is going on. Rahul takes you and other girls to a different city, where you are forced to work in the street.");
                // TODO : Next option Intent - go to helpline page
                Intent helpline = new Intent(Q2Activity.this, HelplineActivity.class);
                startActivity(helpline);

            }
        });

        //Query for records
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceholderApi jsonPlaceholderApi = retrofit.create(JsonPlaceholderApi.class);
        Call<List<GetApp>> call = jsonPlaceholderApi.getApp();

        call.enqueue(new Callback<List<GetApp>>() {
            @Override
            public void onResponse(Call<List<GetApp>> call, Response<List<GetApp>> response) {

                if(!response.isSuccessful()){
//                    tvResult.setText("Code: " +response.code());
                    return;
                }

                List<GetApp> getApps = response.body();
                for(GetApp getApp : getApps) {
                    String content = "";
                    content += "ID: " + getApp.getId() + "\n";
                    content += "Name: " + getApp.getName() + "\n";
                    content += "Description: " + getApp.getDescription();

//                   tvResult.append(content);
                }

            }

            @Override
            public void onFailure(Call<List<GetApp>> call, Throwable t) {
//                tvResult.setText(t.getMessage());

            }
        });
    }
}
