package com.ksl.pc;

import com.ksl.pc.model.GetApp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceholderApi {

    @GET("apps")
    Call<List<GetApp>> getApp();


}
